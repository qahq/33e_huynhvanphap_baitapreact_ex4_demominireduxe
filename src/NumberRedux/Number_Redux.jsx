import React, { Component } from 'react'
import { connect } from 'react-redux'
import { giamSoLuongAction, tangSoLuongAction } from './Redux/Actions/numberActions'

class Number_Redux extends Component {
  render() {
    let {tangSoLuong,giamSoLuong} = this.props
    return (
      <div>
        <button onClick={()=>{
          tangSoLuong(10)
        }} className='btn btn-success'>TĂNG SỐ LƯỢNG</button>
        <span className='display-4 text-secondary m-3'>{this.props.soLuong}</span>
        <button onClick={()=>{
          giamSoLuong(10)
        }} className='btn btn-danger'>GIẢM SỐ LƯỢNG</button>
      </div>
    )
  }
}

let mapStateToProps = (state) =>{
  return {
    soLuong : state.numberReducer.number
  }
}
let mapDispatchToProps =(dispatch)=>{
  return {
    tangSoLuong:(soLuong)=>{
      dispatch(tangSoLuongAction(soLuong))
    },
    giamSoLuong:(soLuong)=>{
      dispatch(giamSoLuongAction(soLuong))
    }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Number_Redux)
