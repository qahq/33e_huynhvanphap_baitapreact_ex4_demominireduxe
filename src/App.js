import logo from './logo.svg';
import './App.css';
import Number_Redux from './NumberRedux/Number_Redux';

function App() {
  return (
    <div className="App">
    <Number_Redux/>
    </div>
  );
}

export default App;
